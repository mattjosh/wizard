"""Simple test case."""

from test import sorcery, async_sorcery

def setup():
    print("setup")

def test_my_cake():
    pass


def test_my_sync_script():
    pass


@async_sorcery(group_id="plsfail")
async def test_my_async_cake():
    pass


async def test_my_async_script():
    raise Exception

@async_sorcery(group_id="plsfail")
async def test_async_failure():
    raise Exception


def clean_up_module():
    print("clean_up_module")


GROUPS = {
    "my_async_one": {
        "concurrency": "asynchronous",
        "functions": [test_my_async_cake, test_my_async_script],
    },
    "my_multi_processing_one": {
        "concurrency": "multiprocessing",
        "functions": [test_my_cake, test_my_sync_script],
    },
    # [...] More groups here.
}
