#!/usr/bin/env python3
""""""
import argparse
import traceback
import asyncio
import importlib
import inspect
import logging
import sys

from pathlib import Path
from functools import reduce


LOG = logging.getLogger(__name__)

def fn_from_exception(e):
    return traceback.extract_tb(e.__traceback__)[-1][2]

def make_null_fixture(name):
    """"""
    def null_fixture(*args, **kwargs):
        LOG.debug("Skipping %s", name)
    return null_fixture

def async_sorcery(group_id=''):
    """ A decorator to categorise tests """
    def real_decorator(function):
        async def wrapper(*args, **kwargs):
            await function(*args, **kwargs)
        wrapper.__group_id = group_id
        wrapper.__name__ = function.__name__
        return wrapper
    return real_decorator

def sorcery(group_id=''):
    """ A decorator to categorise tests """
    def real_decorator(function):
        def wrapper(*args, **kwargs):
            function(*args, **kwargs)
        wrapper.__group_id = group_id
        wrapper.__name__ = function.__name__
        return wrapper
    return real_decorator

def harvest_tests(module):
    """ Returns a dictionary of async and sync tests, keyed by group """
    test_prefix = getattr(module, "test_prefix", "test")
    tests = getattr(module, "tests", filter(callable, [getattr(module, name)
                                                       for name in dir(module)
                                                       if name.startswith(test_prefix)]))
    farm = {}
    for fn in tests:
        group = getattr(fn, '__group_id', "")
        if not farm.get(group):
            farm[group] = {'async': [], 'sync': []}

        exec_id = 'async' if asyncio.iscoroutinefunction(fn) and group else 'sync'
        farm[group][exec_id].append(fn)

    return farm

def run_parallel(tests):
    loop = asyncio.get_event_loop()
    results = loop.run_until_complete(asyncio.gather(*[fn() for fn in tests],
                                                     return_exceptions=True))

    return list(filter(lambda res: isinstance(res, Exception), results))
    

def run_sync(tests):
    failures = []

    loop = asyncio.get_event_loop()
    for test in tests:
        try:
            if asyncio.iscoroutinefunction(test):
                loop.run_until_complete(test())
            else:
                test()
        except Exception as e:
            failures.append(e)
    return failures

# Ha ha ha ha ha good luck
def run_group(tests):
    failures = {fn_from_exception(v): v 
                for v in run_sync(tests['sync']) + run_parallel(tests['async'])}

    return {fn.__name__ : failures.get(fn.__name__, False)
            for fn in reduce(lambda x, y: x + y, tests.values())}

# BUG: Currently not using helper functions, but they need more thought. Do we have a helper fn
#      per group? Probably. How do we use them in async case? Do we require async_ flavours
#      of them? Needs some thought.
def run_tests(farm, module):
    loop = asyncio.get_event_loop()

    results = {}
    for group, tests in farm.items():
        group_result = run_group(tests)
        results[group or 'orphaned'] = group_result

    loop.close()

    return results

def main():
    """"""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("test", help="The python module to run.")
    kwargs = parser.parse_args(sys.argv[1:])
    module = importlib.import_module(Path(kwargs.test).resolve().stem)
    farm = harvest_tests(module)
    results = run_tests(farm, module)
    for k, res in results.items():
        print('Group:', k)
        for key, v in res.items():
            print('Test', key, ':', 'Failed!' if v else 'Passed!')
    return 0

if __name__ == "__main__":
    rc = 1
    try:
        rc = main()
    except Exception as e:
        if LOG.level <= logging.DEBUG:
            raise
        else:
            LOG.error(str(e))
    sys.exit(rc)
